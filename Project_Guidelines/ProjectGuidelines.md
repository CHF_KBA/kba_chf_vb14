**Project Guidelines**

- Select a use case, where applying distributed ledger is relevant. 
- Create a simple pitch deck showing the details and workflow of your project. 
- Create a working demo. 


Your final project contents should have:

1. **The application built using Hyperledger Fabric**: This is the coding part of your project. The code must be clearly commented. And it should have:
- Network Setup
- Chaincode
- The project should implement chaincode features like PDC  and Rich queries(including functions to get history and list of assets).
- The client using Client SDK. The project should also implement Events.
- UI module (This is not necessary if you could show the working using API interfaces)

2. **Readme file**: The readme file of your project repo (Readme.md) should contain the following:
- Name of the project.
- Step-by-step instructions for installing/setting up the application for use.

3. **Pitch deck**: The pitch deck showing the details and workflow of your project.

4. **Documentation**: The documentation file of your project should have the following contents.
- Use case (Current Scenario, Pros & Cons).
- Why Fabric(Advantages of introducing Fabric).
- Rough and neat workflow diagram of the use case.
- Shortcomings and future enhancements.

Sample document is uploaded.

If required you may seek assistance from the KBA Fabric team during the development phase by mail (chf.kba@iiitmk.ac.in) or telegram group.  

**Project Deadlines**

Last date for pitch deck submission: **26-July-2024**

Last date for project submission: **19-Aug-2024**	


**How to submit your project.?**

The medium of submission will be **GitHub classroom**. If you are unfamiliar with this platform, please follow the steps below to submit your project.

First, log in to your github account. Then please go to the given invite link to join the CHF_VB14 classroom: https://classroom.github.com/a/a62iwTey.

After uploading the final version of the project, you should send us an e-mail(**chf.kba@iiitmk.ac.in**)  to notify us about your submission.  On receiving the mail, the KBA team will execute or verify your project and will schedule an online evaluation session.

**General guidelines to be followed for presentation:**
- Join the Zoom meeting using the laptop.
- Login at least 5 minutes before the scheduled time.
- You are supposed to wait until the admin lets you into the Zoom meeting.
- The Laptops should be ready for screen sharing.
- The camera and mic of the device should be on throughout the presentation.
- **Be prepared for the viva that covers the concepts you have learned during the program.**
- Bootstrap the network and do some transactions before the scheduled time.
- Demonstrate the complete working of the project during the presentation.
- Explain the overall working of the project within 10 minutes and 15 minutes is reserved for viva.
- Upload the project in Github classroom. 
- Follow the project guidelines.

