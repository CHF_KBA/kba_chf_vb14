# KBA_CHF_VB14

Course Link: https://elearning.kba.ai/courses/course-v1:KBA+CHF_GO+2023_Q3/course/

Hyperledger Fabric readthedocs: https://hyperledger-fabric.readthedocs.io/en/release-2.5/index.html

Fabric samples: https://github.com/hyperledger/fabric-samples

Hyperledger Fabirc github: https://github.com/hyperledger/fabric

Hyperledger: https://www.hyperledger.org/

Hyperledger India Chapter: https://wiki.hyperledger.org/display/HIRC/Hyperledger+India+Regional+Chapter+Home

Hyperledger Fabric Certified Practitioner (HFCP): https://training.linuxfoundation.org/certification/hyperledger-fabric-certified-practitioner-hfcp/


Feedback link: https://forms.gle/aZwm3mSUZB4gQFhk6

Project Submission link: https://classroom.github.com/a/a62iwTey

Note: Project guidelines uploaded in Project_Guidelines folder






