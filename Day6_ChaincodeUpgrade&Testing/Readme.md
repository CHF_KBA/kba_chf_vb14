
# Start network using script file

```
./startNetwork.sh

```

# Minifab commands to deploy and invoke chaincode

```
sudo chmod -R 777 vars/
```
```
mkdir -p vars/chaincode/KBA-Automobile/go
```
```
cp -r ../Chaincode/* vars/chaincode/KBA-Automobile/go/
```
```
minifab ccup -n KBA-Automobile -l go -v 1.0 -d false -r false
```
```
minifab invoke -n KBA-Automobile -p '"CreateCar","car01","BMW","320d","Red","F-01","01/01/2024"'
```
```
minifab query -n KBA-Automobile -p '"ReadCar","car01"'

```

**Update the chaincode**
```
cp -r ../Chaincode/* vars/chaincode/KBA-Automobile/go/
```

```
minifab ccup -n KBA-Automobile -l go -v 1.1 -d false -r false
```

```
minifab invoke -n KBA-Automobile -p '"DeleteCar","car01"'
```
```
minifab query -n KBA-Automobile -p '"ReadCar","car01"'

```


**Checking access control**
```
minifab invoke -n KBA-Automobile -p '"CreateCar","car02","BMW","320d","Red","F-01","01/01/2024"' -o dealer.auto.com 
```
```
minifab invoke -n KBA-Automobile -p '"CreateCar","car02","BMW","320d","Red","F-01","01/01/2024"' -o manufacturer.auto.com 
```
```
minifab query -n KBA-Automobile -p '"ReadCar","car02"'

```

**To view explorer**
```
minifab explorerup
```
userid: `exploreradmin`

password: `exploreradminpw`
```
minifab explorerdown
```
**To view couchdb**

http://localhost:7006/_utils/

userid: `admin`

password: `adminpw`


**To view the details of a block**
```
minifab blockquery
```
```
minifab blockquery -b 6
```
**To view blockchain**
```
docker exec -it peer1.mvd.auto.com /bin/sh
```
```
ls var/hyperledger/production/ledgersData/chains/chains/autochannel/
```
```
cat var/hyperledger/production/ledgersData/chains/chains/autochannel/blockfile_000000
```
```
exit
```

**To stop network and restart it later**

```
minifab down
```
```
minifab restart
```

**To cleanup entire network**
```
minifab cleanup
```
```
sudo rm -rf vars
```


**Chaincode Tesing**

Create a new folder named test

```
mkdir test
```

Now create a file named car-contract_test.go inside test directory
```
touch test/car-contract_test.go
```

We need to create mocks for all the method for fabric communication
```
go install github.com/maxbrunsfeld/counterfeiter/v6@latest
```
Or

```
git clone https://github.com/maxbrunsfeld/counterfeiter.git
cd counterfeiter
go build
sudo cp counterfeiter /usr/local/go/bin
```

Use this command to add the dependencies required for testing
```
go mod tidy
```

Use the following command to generate mocks
```
go generate
```

Use the following command to run the test files
```
go test
```